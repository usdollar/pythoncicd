echo 'deploy app to server PROD: =======>'
rm -fr $HOME/pythoncicd
git clone https://gitlab.com/usdollar/pythoncicd.git
cd $HOME/pythoncicd
docker stack deploy --compose-file docker-compose-prod.yml stackpython
echo '=====> deploy success on PROD server'
